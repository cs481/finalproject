﻿using System;
using System.IO;
using FinalProject.DependencyServices;
using FinalProject.Droid.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileAccessServiceAndroid))]
namespace FinalProject.Droid.DependencyServices
{
    public class FileAccessServiceAndroid : IFileAccessService
    {
        public string GetSqLiteDatabasePath(string databaseName)
        {
            string personalFolderPath = Environment.GetFolderPath(
                Environment.SpecialFolder.Personal);
            var dbPath = Path.Combine(personalFolderPath, databaseName);
            Console.WriteLine($"**** {this.GetType().Name}.{nameof(GetSqLiteDatabasePath)}:  returning:[{dbPath}]");
            return dbPath;
        }
    }
}
