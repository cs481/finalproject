﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using FinalProject.DependencyServices;
using FinalProject.Droid.DependencyServices;
using Prism;
using Prism.Ioc;

[assembly: Xamarin.Forms.ExportRenderer(typeof(Xamarin.Forms.Button), typeof(FinalProject.Droid.CustomButtonRenderer))]
namespace FinalProject.Droid
{
    [Activity(Label = "FinalProject", Icon = "@mipmap/ic_launcher", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App(new AndroidInitializer()));
        }
    }

    public class AndroidInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry container)
        {
            // Register any platform specific implementations
            container.RegisterSingleton<IFileAccessService, FileAccessServiceAndroid>();
        }
    }

    class CustomButtonRenderer : Xamarin.Forms.Platform.Android.ButtonRenderer
    {
        public CustomButtonRenderer(Android.Content.Context context) : base(context)
        {
        }
    }
}

