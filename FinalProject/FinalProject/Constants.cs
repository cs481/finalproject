﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProject
{
    class Constants
    {
        public static string FavoriteImageUrl = "ic_star.png";
        public static string BorderFavoriteImageUrl = "ic_star_border.png";

        public static string ErrorEmptyFieldsCustomJokes = "You need to fill both first name and last name fields";
        public static string ErrorNoInternetConnection = "Cannot find internet connection.Try again later.";

        public static string Error = "Error";
        public static string OK = "OK";

        public static string Joke = "Joke";

        public static string TutoTitle = "End tutorial";
        public static string TutoMsg = "Do you want to display this tutorial next time ?";
        public static string Yes = "Yes";
        public static string No = "No";
    }
}
