﻿using Prism;
using Prism.Ioc;
using FinalProject.ViewModels;
using FinalProject.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism.Unity;
using System.Diagnostics;
using FinalProject.Services;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace FinalProject
{
    public partial class App : PrismApplication
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnInitialized)}");

            InitializeComponent();

            await NavigationService.NavigateAsync($"{nameof(NavigationPage)}/{nameof(MainPage)}");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RegisterTypes)}");

            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<AboutPage, AboutPageViewModel>();
            containerRegistry.RegisterForNavigation<TabContainerPage, TabContainerPageViewModel>();
            containerRegistry.RegisterForNavigation<CustomJokesPage, CustomJokesPageViewModel>();
            containerRegistry.RegisterForNavigation<FavoriteJokesPage, FavoriteJokesPageViewModel>();
            containerRegistry.RegisterForNavigation<RandomJokesPage, RandomJokesPageViewModel>();
            containerRegistry.RegisterForNavigation<TutorialPage, TutorialPageViewModel>();
            containerRegistry.RegisterForNavigation<DetailJokePage, DetailJokePageViewModel>();
            containerRegistry.RegisterSingleton<IChuckRepository, ChuckRepository>();
        }
    }
}
