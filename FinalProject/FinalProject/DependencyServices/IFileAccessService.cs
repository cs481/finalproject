﻿using System;
namespace FinalProject.DependencyServices
{
    public interface IFileAccessService
    {
        string GetSqLiteDatabasePath(string databaseName);
    }
}
