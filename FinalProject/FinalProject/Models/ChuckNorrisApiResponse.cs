﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using FinalProject.Models;
//
//    var chuckNorrisApiResponse = ChuckNorrisApiResponse.FromJson(jsonString);

namespace FinalProject.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class ChuckNorrisApiResponse
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public Value Value { get; set; }
    }

    public partial class Value
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("joke")]
        public string Joke { get; set; }

        [JsonProperty("categories")]
        public List<object> Categories { get; set; }
    }

    public partial class ChuckNorrisApiResponse
    {
        public static ChuckNorrisApiResponse FromJson(string json) => JsonConvert.DeserializeObject<ChuckNorrisApiResponse>(json, FinalProject.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this ChuckNorrisApiResponse self) => JsonConvert.SerializeObject(self, FinalProject.Models.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
