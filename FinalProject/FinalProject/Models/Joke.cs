﻿using System;
using SQLite;

namespace FinalProject.Models
{
    [Table("joke")]
    public class Joke
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [MaxLength(1000)]
        public string Text { get; set; }

        [Unique]
        public long Idapi { get; set; }

        public Joke()
        {
        }
    }
}
