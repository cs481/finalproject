﻿using FinalProject.Models;
using FinalProject.Services;
using FinalProject.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace FinalProject.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        INavigationService _navigationService;
        private IChuckRepository _chuckRepository;

        public DelegateCommand EnterCommand { get; set; }
        public DelegateCommand AboutCommand { get; set; }

        private ObservableCollection<Joke> _jokes;
        public ObservableCollection<Joke> Jokes
        {
            get { return _jokes;  }
            set { SetProperty(ref _jokes, value); }
        }

        public MainPageViewModel(INavigationService navigationService, IChuckRepository chuckRepository) 
            : base (navigationService)
        {
            Title = "Joke Generator";
            _navigationService = navigationService;
            EnterCommand = new DelegateCommand(OnEnter);
            AboutCommand = new DelegateCommand(OnAbout);

            _chuckRepository = chuckRepository;
        }


        private void OnAbout()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAbout)}");
            _navigationService.NavigateAsync(nameof(AboutPage));
        }

        private async void OnEnter()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnEnter)}");
            if (PrefService.Tutorial == true)
            {
                await _navigationService.NavigateAsync(nameof(TutorialPage));
            }
            else
            {
                await _navigationService.NavigateAsync(nameof(TabContainerPage));
            }
        }
    }
}
