﻿using FinalProject.Models;
using FinalProject.Services;
using FinalProject.Views;
using Prism;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;

namespace FinalProject.ViewModels
{
    class FavoriteJokesPageViewModel : BindableBase, IActiveAware
    {
        private IChuckRepository _chuckRepository;
        private INavigationService _navigationService;
        public DelegateCommand<Joke> JokeTappedCommand { get; set; }
        public DelegateCommand OnPullToRefreshCommand { get; set; }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public event EventHandler IsActiveChanged;

        private bool _showBusySpinner;
        public bool ShowBusySpinner
        {
            get { return _showBusySpinner; }
            set
            {
                SetProperty(ref _showBusySpinner, value);
            }
        }

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                SetProperty(ref _isActive, value);
                IsActiveChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private ObservableCollection<Joke> _jokes;
        public ObservableCollection<Joke> Joke
        {
            get { return _jokes; }
            set { SetProperty(ref _jokes, value); }
        }

        private Joke _jokeTapped;
        public Joke JokeTapped
        {
            get { return _jokeTapped; }
            set { SetProperty(ref _jokeTapped, value); }
        }

        public FavoriteJokesPageViewModel(IChuckRepository chuckRepository, INavigationService navigationService)
        {
            Title = "Favorite";
            IsActiveChanged += OnIsActiveChanged;
            _chuckRepository = chuckRepository;
            ShowBusySpinner = false;
            _navigationService = navigationService;
            JokeTappedCommand = new DelegateCommand<Joke>(OnJokeTapped);
            OnPullToRefreshCommand = new DelegateCommand(OnPullToRefresh);
        }

        private void OnPullToRefresh()
        {
            RefreshJokes();
        }

        private async void OnJokeTapped(Joke jokeTapped)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnJokeTapped)}");
            NavigationParameters navigationParam = new NavigationParameters();
            navigationParam.Add(Constants.Joke, jokeTapped);
            await _navigationService.NavigateAsync(nameof(DetailJokePage), navigationParam, false, true);
        }

        private async void RefreshJokes()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RefreshJokes)}");
            Console.WriteLine($"**** {this.GetType().Name}.{nameof(RefreshJokes)}");
            ShowBusySpinner = true;
            Joke = await _chuckRepository.GetAllJoke();
            ShowBusySpinner = false;
        }

        void OnIsActiveChanged(object sender, EventArgs emptyEventArgs)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnIsActiveChanged)}: {IsActive}");
            getJokes();
        }

        private async void getJokes() 
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(getJokes)}");
            ShowBusySpinner = true;
            Joke = await _chuckRepository.GetAllJoke();
            ShowBusySpinner = false;
            foreach (Joke joke in Joke)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(getJokes)}: Joke[{joke.Text}]");
            }
        }

    }
}
