﻿using Prism;
using Prism.Mvvm;
using System;
using System.Diagnostics;
using FinalProject.Services;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Services;
using Plugin.Vibrate;
using Xamarin.Forms;
using FinalProject.Models;

namespace FinalProject.ViewModels
{
    class RandomJokesPageViewModel : BindableBase, IActiveAware
    {
        public DelegateCommand GetJokeCommand { get; set; }
        public DelegateCommand FavoriteCommand { get; set; }

        private bool isFavorite = true;

        private IChuckRepository _chuckRepository;

        public bool IsRequestProcessing { get; private set; }

        private Joke _joke;
        public Joke Joke
        {
            get { return _joke; }
            set { SetProperty(ref _joke, value); }
        }

        private string _favorite;
        public string Favorite
        {
            get { return _favorite; }
            set { SetProperty(ref _favorite, value); }
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private ApiService _apiService;

        public event EventHandler IsActiveChanged;

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                SetProperty(ref _isActive, value);
                IsActiveChanged?.Invoke(this, EventArgs.Empty);
            }
        }


       
        public RandomJokesPageViewModel(IPageDialogService dialogService, IChuckRepository chuckRepository)
        {
            Title = "Random";
            _apiService = new ApiService(dialogService);
            _chuckRepository = chuckRepository;
            IsActiveChanged += OnIsActiveChanged;
            GetJokeCommand = new DelegateCommand(GetJoke);
            FavoriteCommand = new DelegateCommand(OnFavorite);
        }

        private async void OnFavorite()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnFavorite)}");
            isFavorite = !isFavorite;

            var sliderVibrate = new Xamarin.Forms.Slider(0, 10000.0, 500.0);
            CrossVibrate.Current.Vibration(TimeSpan.FromMilliseconds((int)sliderVibrate.Value));

            if (isFavorite)
            {
                await _chuckRepository.SaveJoke(Joke);
                Favorite = Constants.FavoriteImageUrl;
            }
            else
            {
                await _chuckRepository.DeleteJoke(Joke);
                Favorite = Constants.BorderFavoriteImageUrl;
            }
        }

        void OnIsActiveChanged(object sender, EventArgs emptyEventArgs)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnIsActiveChanged)}: {IsActive}");
            GetJoke();
        }

        private async void GetJoke()
        {
            await GetJokeAsync();
        }

        private async Task GetJokeAsync()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}");
            IsRequestProcessing = true;
            Joke = await _apiService.GetJokeAsync();
            //TODO: Check if belong to favorite
            isFavorite = false;
            Favorite = Constants.BorderFavoriteImageUrl;
            IsRequestProcessing = false;
        }
    }
}
