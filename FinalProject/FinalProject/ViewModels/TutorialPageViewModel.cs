﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using FinalProject.Services;
using FinalProject.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;

namespace FinalProject.ViewModels
{
    class TutorialPageViewModel : BindableBase, INavigationAware
    {
        public DelegateCommand EndTutorialCommand { get; set; }
        private IPageDialogService _pageDialogService;
        private INavigationService _navigationService;

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _endTutorial;
        public string EndTutorial
        {
            get { return _endTutorial; }
            set { SetProperty(ref _endTutorial, value); }
        }

        public TutorialPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnEndTutorial)}");
            Title = "Tutorial";
            EndTutorial = Constants.OK;
            EndTutorialCommand = new DelegateCommand(OnEndTutorial);
            _pageDialogService = pageDialogService;
            _navigationService = navigationService;
        }

        private async void OnEndTutorial()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnEndTutorial)}");
            bool userResponse = await _pageDialogService.DisplayAlertAsync(Constants.TutoTitle,
                                                                                   Constants.TutoMsg,
                                                                                   Constants.Yes,
                                                                                   Constants.No);

            if (userResponse == false)
            {
                PrefService.Tutorial = false;
            }
            await _navigationService.NavigateAsync(nameof(TabContainerPage));
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedFrom)}");
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedTo)}");
        }

        public async void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}");
        }
    }
}
