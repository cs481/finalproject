﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace FinalProject.ViewModels
{
        public class TabContainerPageViewModel : BindableBase
        {
            private string _title;
            public string Title
            {
                get { return _title; }
                set { SetProperty(ref _title, value); }
            }

            public TabContainerPageViewModel()
            {
                 Debug.WriteLine($"**** {this.GetType().Name}.{nameof(TabContainerPageViewModel)}:  ctor");
                Title = "Joke Generator";
            }
        }
}
