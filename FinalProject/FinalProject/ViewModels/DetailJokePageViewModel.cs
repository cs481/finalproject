﻿using System;
using System.Diagnostics;
using FinalProject.Models;
using FinalProject.Services;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace FinalProject.ViewModels
{
    public class DetailJokePageViewModel : Prism.Mvvm.BindableBase, INavigationAware
    {
        public Prism.Commands.DelegateCommand FavoriteCommand { get; set; }

        private bool isFavorite = true;
        private IChuckRepository _chuckRepository;

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private Joke _joke;
        public Joke Joke
        {
            get { return _joke; }
            set { SetProperty(ref _joke, value); }
        }

        private string _favorite;
        public string Favorite
        {
            get { return _favorite; }
            set { SetProperty(ref _favorite, value); }
        }

        private bool _isActive;
        private IPageDialogService _pageDialogService;

        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                SetProperty(ref _isActive, value);
                IsActiveChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public event EventHandler IsActiveChanged;

        public DetailJokePageViewModel(IChuckRepository chuckRepository, IPageDialogService pageDialogService)
        {
            Title = "Detail Joke";
            _chuckRepository = chuckRepository;
            FavoriteCommand = new Prism.Commands.DelegateCommand(OnFavorite);
            _pageDialogService = pageDialogService;
        }


        public void OnNavigatedFrom(NavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            Joke = parameters.GetValue<Joke>(Constants.Joke);
            Favorite = Constants.FavoriteImageUrl;
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            Joke = parameters.GetValue<Joke>(Constants.Joke);
            Favorite = Constants.FavoriteImageUrl;
        }

        private async void OnFavorite()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnFavorite)}");
            isFavorite = !isFavorite;
            if (isFavorite)
            {
                await _chuckRepository.SaveJoke(Joke);
                Favorite = Constants.FavoriteImageUrl;
            }
            else
            {
                bool userResponse = await _pageDialogService.DisplayAlertAsync("Unfavorite",
                                                                                "Are you sure to unfavorite this joke ?",
                                                                                     Constants.Yes,
                                                                                    Constants.No);

                if (userResponse == true)
                {
                    await _chuckRepository.DeleteJoke(Joke);
                    Favorite = Constants.BorderFavoriteImageUrl;
                }
            }
        }

        void OnIsActiveChanged(object sender, EventArgs emptyEventArgs)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnIsActiveChanged)}: {IsActive}");
        }

    }
}

