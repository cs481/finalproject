﻿using Prism;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace FinalProject.ViewModels
{
    class AboutPageViewModel : BindableBase
    {
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public event EventHandler IsActiveChanged;

        public AboutPageViewModel()
        {
            Title = "About";
        }
    }
}
