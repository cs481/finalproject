﻿using Prism;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Diagnostics;
using FinalProject.Services;
using System.Threading.Tasks;
using Prism.Services;
using FinalProject.Models;

namespace FinalProject.ViewModels
{
    class CustomJokesPageViewModel : BindableBase, IActiveAware
    {
        private ApiService _apiService;
        private IPageDialogService _dialogService;

        public DelegateCommand GetJokeCommand { get; set; }
        public DelegateCommand FavoriteCommand { get; set; }

        private bool isFavorite = false;
        private IChuckRepository _chuckRepository;

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public event EventHandler IsActiveChanged;

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                SetProperty(ref _isActive, value);
                IsActiveChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        private string _firstNameField;
        public string FirstNameField
        {
            get { return _firstNameField; }
            set { SetProperty(ref _firstNameField, value); }
        }

        private string _lastNameField;
        public string LastNameField
        {
            get { return _lastNameField; }
            set { SetProperty(ref _lastNameField, value); }
        }

        private Joke _joke;
        public Joke Joke
        {
            get { return _joke; }
            set { SetProperty(ref _joke, value); }
        }

        private string _favorite;
        public string Favorite
        {
            get { return _favorite; }
            set { SetProperty(ref _favorite, value); }
        }

        public bool IsRequestProcessing { get; private set; }

        public CustomJokesPageViewModel(IPageDialogService dialogService, IChuckRepository chuckRepository)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(CustomJokesPageViewModel)} : ctor");
            Title = "Custom";
            IsActiveChanged += OnIsActiveChanged;
            _apiService = new ApiService(dialogService);
            _dialogService = dialogService;
            _chuckRepository = chuckRepository;
            GetJokeCommand = new DelegateCommand(OnGetJoke);
            FavoriteCommand = new DelegateCommand(OnFavorite);
        }

        private async void OnFavorite()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnFavorite)}");
            isFavorite = !isFavorite;
            if (isFavorite)
            {
                await _chuckRepository.SaveJoke(Joke);
                Favorite = Constants.FavoriteImageUrl;
            }
            else
            {
                await _chuckRepository.DeleteJoke(Joke);
                Favorite = Constants.BorderFavoriteImageUrl;
            }
        }

        private async void OnGetJoke()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnGetJoke)}");
            if (FirstNameField != null && LastNameField != null)
            {
                await GetJokeAsync();
            }
            else
            {
                await _dialogService.DisplayAlertAsync(Constants.Error, Constants.ErrorEmptyFieldsCustomJokes, Constants.OK);
            }
        }

        private async Task GetJokeAsync()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}");
            IsRequestProcessing = true;
            Joke = await _apiService.GetJokeAsync(FirstNameField, LastNameField);
            IsRequestProcessing = false;
            isFavorite = false;
            Favorite = Constants.BorderFavoriteImageUrl;
        }

        void OnIsActiveChanged(object sender, EventArgs emptyEventArgs)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnIsActiveChanged)}: {IsActive}");
        }
    }
}