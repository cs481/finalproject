﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using FinalProject.DependencyServices;
using FinalProject.Models;
using SQLite;

namespace FinalProject.Services
{
    public class ChuckRepository : IChuckRepository
    {
        private IFileAccessService _fileAccessService;
        private SQLiteAsyncConnection _sqliteConnection;

        public ChuckRepository(IFileAccessService fileAccessService)
        {
            _fileAccessService = fileAccessService;
            var databaseFilePath = _fileAccessService.GetSqLiteDatabasePath("chuck.db3");
            _sqliteConnection = new SQLiteAsyncConnection(databaseFilePath);
            CreateTablesSynchronously();

            Console.WriteLine($"**** {this.GetType().Name}.{nameof(ChuckRepository)}: ctor. databaseFilePath={databaseFilePath}");
        }

        private void CreateTablesSynchronously()
        {
            Console.WriteLine($"**** {this.GetType().Name}.{nameof(CreateTablesSynchronously)}");

            _sqliteConnection.CreateTableAsync<Joke>().Wait();        
        }

        public async Task<int> SaveJoke(Joke jokeToSave)
        {
            Console.WriteLine($"**** {this.GetType().Name}.{nameof(SaveJoke)}:  {jokeToSave}");

            try
            {
                if (jokeToSave.Id == 0) // an Id of 0 indicates a new item, not yet saved.
                {
                    jokeToSave.Id = await _sqliteConnection.InsertAsync(jokeToSave);
                }
                else
                {
                    await _sqliteConnection.UpdateAsync(jokeToSave);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"**** {this.GetType().Name}.{nameof(SaveJoke)}:  EXCEPTION!! {ex}");
            }

            return jokeToSave.Id;
        }

        public async Task<ObservableCollection<Joke>> GetAllJoke()
        {
            var allJoke = new ObservableCollection<Joke>();

            try
            {
                List<Joke> jokeList = await _sqliteConnection.Table<Joke>().ToListAsync();
                allJoke = new ObservableCollection<Joke>(jokeList);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"**** {this.GetType().Name}.{nameof(GetAllJoke)}:  EXCEPTION!! {ex}");
            }

            return allJoke;
        }

        public async Task<bool> DeleteJoke(Joke jokeToDelete)
        {
            Console.WriteLine($"**** {this.GetType().Name}.{nameof(DeleteJoke)}:  {jokeToDelete}");

            try
            {
                if (jokeToDelete.Id == 0) // an Id of 0 indicates a new item, not yet saved.
                {
                    return false;
                }
                else
                {
                    int res = await _sqliteConnection.DeleteAsync(jokeToDelete);
                    return res == 1 ? true : false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"**** {this.GetType().Name}.{nameof(DeleteJoke)}:  EXCEPTION!! {ex}");
            }
            return false;
        }
    }
}
