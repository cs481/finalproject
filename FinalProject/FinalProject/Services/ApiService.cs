﻿using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using FinalProject.Models;
using System;
using System.Diagnostics;
using Prism.Services;

namespace FinalProject.Services
{
    public class ApiService
    {
        private string _baseUrl = "https://api.icndb.com/jokes/random";
        private string _apiFirstSuffix = $"?firstName=";
        private string _apiLastSuffix = $"&lastName=";

        private HttpClient _httpClient;
        private IPageDialogService _dialogService;

        public ApiService(IPageDialogService dialogService)
        {
            _httpClient = new HttpClient();
            _httpClient.Timeout = TimeSpan.FromSeconds(5);

            _dialogService = dialogService;
        }

        public bool DoIHaveInternet()
        {
            if (!CrossConnectivity.IsSupported)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(DoIHaveInternet)}:  Not supported... Returning true no matter what.");
                return true;
            }

            bool hasNet = CrossConnectivity.Current.IsConnected;
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(DoIHaveInternet)}:  returning {hasNet}");
            return hasNet;
        }

        public async Task<Joke> GetJokeAsync()
        {
            if (DoIHaveInternet() == false)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}:  Returning null... We do not have internet.");
                await _dialogService.DisplayAlertAsync(Constants.Error, Constants.ErrorNoInternetConnection, Constants.OK);
                return null;
            }

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri($"{_baseUrl}");
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}: Sending Request:{request.RequestUri.ToString()}");

            Joke joke = new Joke();
            try
            {
                using (HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(request))
                {
                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}: Request Failed. Status code:{httpResponseMessage.StatusCode}");
                        return null;
                    }
                    string responseAsString = await httpResponseMessage.Content.ReadAsStringAsync();
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}: json response:\n{responseAsString}");

                    ChuckNorrisApiResponse resultDeserialized = JsonConvert.DeserializeObject<ChuckNorrisApiResponse>(responseAsString);
                    joke.Text = resultDeserialized.Value.Joke.Replace("&quot;", "\"");
                    joke.Idapi = resultDeserialized.Value.Id;
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}: json response formatted:\n{joke}");
                    return joke;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}: EXcEPTION:{ex}");
            }

            return joke;
        }

        public async Task<Joke> GetJokeAsync(string firstName, string lastName)
        {
            if (DoIHaveInternet() == false)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}:  Returning null... We do not have internet.");
                await _dialogService.DisplayAlertAsync(Constants.Error, Constants.ErrorNoInternetConnection, Constants.OK);
                return null;
            }

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri($"{_baseUrl}");
            request.RequestUri = new Uri($"{_baseUrl}{_apiFirstSuffix}{firstName}{_apiLastSuffix}{lastName}");
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}: Sending Request:{request.RequestUri.ToString()}");

            Joke joke = new Joke();
            try
            {
                using (HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(request))
                {
                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}: Request Failed. Status code:{httpResponseMessage.StatusCode}");
                        return null;
                    }
                    string responseAsString = await httpResponseMessage.Content.ReadAsStringAsync();
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}: json response:\n{responseAsString}");

                    ChuckNorrisApiResponse resultDeserialized = JsonConvert.DeserializeObject<ChuckNorrisApiResponse>(responseAsString);
                    joke.Text = resultDeserialized.Value.Joke.Replace("&quot;", "\"");
                    joke.Idapi = resultDeserialized.Value.Id;
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}: json response formatted:\n{joke}");
                    return joke;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetJokeAsync)}: EXcEPTION:{ex}");
            }

            return joke;
        }
    }
}