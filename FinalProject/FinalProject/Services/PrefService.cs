﻿using System;
using System.Diagnostics;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace FinalProject.Services
{
    public class PrefService
    {
        private static ISettings AppSettings => CrossSettings.Current;

        public static string Joke
        {
            get {
                Debug.WriteLine($"**** {nameof(Joke)}: Get");
                return AppSettings.GetValueOrDefault(nameof(Joke), string.Empty);
            }

            set {
                Debug.WriteLine($"**** {nameof(Joke)}: Set");
                AppSettings.AddOrUpdateValue(nameof(Joke), value);
            }
        }
        public static void RemoveJoke(string Joke)
        {
            Debug.WriteLine($"**** {nameof(RemoveJoke)}");
            AppSettings.Remove(nameof(Joke));
        }

        public static bool Tutorial
        {
            get
            {
                Debug.WriteLine($"**** {nameof(Tutorial)}: Get");
                return AppSettings.GetValueOrDefault(nameof(Tutorial), true);
            }

            set
            {
                Debug.WriteLine($"**** {nameof(Tutorial)}: Set");
                AppSettings.AddOrUpdateValue(nameof(Tutorial), value);
            }
        }
        public static void RemoveTutorial(bool Tutorial)
        {
            Debug.WriteLine($"**** {nameof(RemoveTutorial)}");
            AppSettings.Remove(nameof(Tutorial));
        }
    }
}
