﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using FinalProject.Models;

namespace FinalProject.Services
{
    public interface IChuckRepository
    {
        Task<int> SaveJoke(Joke jokeToSave);
        Task<ObservableCollection<Joke>> GetAllJoke();
        Task<bool> DeleteJoke(Joke jokeToDelete);
    }
}